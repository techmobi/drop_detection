/**
 * Copyright (c) 2015 - 2020, Nordic Semiconductor ASA
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 *
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 *
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 *
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/** @file
 * @defgroup tw_sensor_example main.c
 * @{
 * @ingroup nrf_twi_example
 * @brief TWI Sensor Example main file.
 *
 * This file contains the source code for a sample application using TWI.
 *
 */

#include <stdio.h>
#include "boards.h"
#include "app_util_platform.h"
#include "app_error.h"
#include "nrf_drv_twi.h"
#include "nrf_delay.h"


#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

/* TWI instance ID. */
#define TWI_INSTANCE_ID     0

/* Common addresses definition for temperature, accelerometer sensor. */
#define MPU6050_ADDR        0x68U

#define ACCEL_CONFIG        0x1CU
#define ACCEL_XOUT_H        0x3BU
#define ACCEL_XOUT_L        0x3CU
#define ACCEL_YOUT_H        0x3DU
#define ACCEL_YOUT_L        0x3EU
#define ACCEL_ZOUT_H        0x3FU
#define ACCEL_ZOUT_L        0x40U
#define TEMP_OUT_H          0x41U
#define TEMP_OUT_L          0x42U
#define SIGNAL_PATH_RESET   0x6BU
#define WHO_AM_I            0x75U


/* TWI instance. */
static const nrf_drv_twi_t m_twi = NRF_DRV_TWI_INSTANCE(TWI_INSTANCE_ID);

/* Buffer for samples read from temperature, accelerometer sensor. */

static uint8_t value;
static int16_t temp;
static int16_t acc_x;
static int16_t acc_y;
static int16_t acc_z;


/**
 * @brief UART initialization.
 */
void twi_init (void)
{
    ret_code_t err_code;

    const nrf_drv_twi_config_t twi_mpu6050_config = {
       .scl                = ARDUINO_SCL_PIN,
       .sda                = ARDUINO_SDA_PIN,
       .frequency          = NRF_DRV_TWI_FREQ_400K,
       .interrupt_priority = APP_IRQ_PRIORITY_HIGH,
       .clear_bus_init     = false
    };

    err_code = nrf_drv_twi_init(&m_twi, &twi_mpu6050_config, NULL, NULL);
    APP_ERROR_CHECK(err_code);

    nrf_drv_twi_enable(&m_twi);
}


/**
 * @brief Function to set the value in the MPU6050 register.
 */
static void set_reg_mpu6050(uint8_t reg, uint8_t value)
{
    ret_code_t err_code;

    nrf_delay_us(100);
    uint8_t data[2] = {reg, value};
    err_code = nrf_drv_twi_tx(&m_twi, MPU6050_ADDR, data, sizeof(data), false);
    APP_ERROR_CHECK(err_code);
}


/**
 * @brief Function to get the value in the MPU6050 register.
 */
static void get_reg_mpu6050(uint8_t reg, uint8_t *value, uint8_t size)
{
    ret_code_t err_code;

    nrf_delay_us(100);
    err_code = nrf_drv_twi_tx(&m_twi, MPU6050_ADDR, &reg, 1, false);
    APP_ERROR_CHECK(err_code);

    nrf_delay_us(100);
    err_code = nrf_drv_twi_rx(&m_twi, MPU6050_ADDR, value, size);
    APP_ERROR_CHECK(err_code);
}


/**
 * @brief Function for reading data from temperature sensor.
 */
static int16_t read_temperature(void)
{
    uint8_t temp[2];
    int16_t temperature;

    get_reg_mpu6050(TEMP_OUT_H, temp, sizeof(temp));

    temperature = temp[0] << 8 | temp[1];
    temperature = temperature/340 + 36.53;
    return temperature;
}


/**
 * @brief Function for reading data from accelerometer sensor.
 */
static void read_acc(int16_t * acc_x, int16_t * acc_y, int16_t * acc_z)
{
    uint8_t acc[6];

    get_reg_mpu6050(ACCEL_XOUT_H, acc, sizeof(acc));
    nrf_delay_us(100);
    *acc_x = acc[0] << 8 | acc[1];
    *acc_y = acc[2] << 8 | acc[3];
    *acc_z = acc[4] << 8 | acc[5];
}


/**
 * @brief Function for main application entry.
 */
int main(void)
{
    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    NRF_LOG_INFO("\r\nTWI sensor example started.");

    twi_init();
    nrf_gpio_cfg_output(24);
    nrf_gpio_pin_clear(24);

    nrf_delay_ms(500);

    set_reg_mpu6050(SIGNAL_PATH_RESET, 0x00);
    set_reg_mpu6050(ACCEL_CONFIG, 0x08);

    get_reg_mpu6050(WHO_AM_I, &value, sizeof(value));
    NRF_LOG_INFO("\r\nvAddress %d", value);
    NRF_LOG_FLUSH();

    while (true)
    {
        nrf_delay_ms(1);

        temp = read_temperature();

        read_acc(&acc_x, &acc_y, &acc_z);
        NRF_LOG_INFO("%d , %d , %d , %d", acc_x, acc_y, acc_z, temp);

        NRF_LOG_FLUSH();
    }
}

/** @} */
