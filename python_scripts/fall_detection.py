import requests
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime

data_in = pd.read_csv("../measurement/data4.csv")
#print(data_in)

X = data_in.loc[:, 'X']
Y = data_in.loc[:, 'Y']
Z = data_in.loc[:, 'Z']

# def find_anomalies(series, value, step):
#     indexes_to_remove = []
#     series_anomalies = []
#     for i in range(series.size-1):
#         if abs(series[i+1] - series[i]) > step :
#             anomaly = {
#                 'date': data_in.loc[i+1, 'DATE'],
#                 'value': data_in.loc[i+1, value],
#                 }
#             series_anomalies.append(anomaly)
#             indexes_to_remove.append(i+1)
#     series_new = series.drop(indexes_to_remove)
#     return series_new, series_anomalies

def threshold_method(X, Y, Z, threshold):
    fall_record = []
    sample_number = ""
    date = ""
    X = X/8192
    Y = Y/8192
    Z = Z/8192
    for i in range(Z.size-1):
        if abs(Z[i+1] - Z[i]) > threshold :
            sample_number = str(i)
            date = str(datetime.date(datetime.now()))
    print(sample_number)
    print(date)
    fall = [date, sample_number]
    fall_record.append(fall)
    return fall_record
fall_record = threshold_method(X, Y, Z, 10)
print(fall_record)

# plt.figure()
# plt.plot(X, 'r--', Y, 'g--', Z, 'b--')
# plt.title("Wykres współrzędnych akcelerometru")
# plt.xlabel("Numer próbki")
# plt.ylabel("Krotność przyspieszenia ziemskiego [g]")
# plt.legend(["Oś X", "Oś Y", "Oś Z"])
# plt.show()

columns = ( "Data wystąpienia upadku", "Numer próbki")
fig, (ax, tabax) = plt.subplots(nrows=2)

ax.plot(X, 'r--', Y, 'g--', Z, 'b--')
ax.set_title("Wykres współrzędnych akcelerometru")
ax.set_xlabel("Numer próbki")
ax.set_ylabel("Krotność przyspieszenia ziemskiego [g]")
ax.legend(["Oś X", "Oś Y", "Oś Z"])

tabax.axis("off")
the_table = tabax.table(cellText=fall_record,
                        colLabels=columns,
                        loc='center')
plt.subplots_adjust(bottom=0.05)
plt.show()